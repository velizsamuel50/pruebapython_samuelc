print("¿Que desea hacer?")
print("")
print("Marca 1 para invertir una cadena de texto")
print("")
print("Marca 2 para invertir una palabra")
seleccion = int(input())

def revertirText():
    class revertirtexto:
        def invertir(self, texto):
            textoI = ' '.join(reversed(texto.split()))
            return textoI
            
    print("Diga su texto para invertirlo...")
    texto = input()
    revertir = revertirtexto()
    print("Aca tiene su texto: ", revertir.invertir(texto))

def revertirPalabra():
    print("Diga su palabra para invertirlo...")
    palabra = input()
    palabraI = ''.join(reversed(palabra))
    print("Aca tiene su texto: ", palabraI)

if seleccion == 1:
    revertirText()
    
if seleccion == 2:
    revertirPalabra()

if seleccion < 1 or seleccion > 2:
    print("Dije 1 o 2, no inventes")

