class calculadora:
    def __init__(self, numero1, numero2):
        self.numero1 = numero1
        self.numero2 = numero2
        
    def suma(self):
        
        return self.numero1 + self.numero2
    
    def resta(self):
        
        return self.numero1 - self.numero2
    
    def multi(self):
        
        return self.numero1 * self.numero2
    
    def dividir(self):
        
        return self.numero1 / self.numero2

print("Indique el primer valor")
num1 = int(input())
print("Indique el segundo valor")
num2 = int(input())

calculo = calculadora(num1,num2)

print("¿Que desea hacer con esos datos?")
print("")
print("1- Sumar, 2- restar, 3- multiplicar, 4- dividir")

eleccion = int(input())

if eleccion == 1:

    print("La suma total es: ", calculo.suma())

if eleccion == 2:

    print("La resta total es: ", calculo.resta())
    
if eleccion == 3:

    print("El total es: ", calculo.multi())

if eleccion == 4:

    print("El total es: ", calculo.dividir())
